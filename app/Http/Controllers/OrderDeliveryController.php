<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderDeliveryController extends Controller
{
    public function picked($id){
        $user = auth()->user();

        $order = Order::find($id);
        $order->deliverer_id= $user->id;
        $order->status = 'picked';
        


        $order->save();
        return redirect('runner_dashboard');
    }

    public function delivered($id){

        $order = Order::find($id);
        $order->status = 'delivered';
        $order->deliver_at = now();
        

        $order->save();
        return redirect('runner_dashboard');
    }

}
