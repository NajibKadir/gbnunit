<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class AdminController extends Controller{
  
    public function dashboard() {
        $orders = Order::all();
    
    
        return view('dashboard.admin_dashboard', [
          'orders' => $orders,
          ]);
        }

        public function search(Request $request){
            $search = $request->get('search');
            $orders = Order::where('pickup_address','like', '%'.$search.'%')->orWhere('delivery_address','LIKE','%'.$search.'%')->orWhere('status','LIKE','%'.$search.'%')
            ->orWhere('phone_no','LIKE','%'.$search.'%')->get();
            if(count($orders)>0){
              return view('dashboard/admin_dashboard',['orders' => $orders])->withQuery('$search');
            }else {
              return view('dashboard/admin_dashboard',['orders' => $orders])->withMessage('No data found.');
            }
            }
}

