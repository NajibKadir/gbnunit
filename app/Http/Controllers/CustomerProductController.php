<?php

namespace App\Http\Controllers;

use App\Product;

class CustomerProductController extends Controller
{
    /** 
     * THis function is used to show the listing for customer
     * to select which product they want to buy
     */
    public function index()
    {
        $products = Product::all();
        return view('customers.products.index', [
            'products' => $products
        ]);
    }
}