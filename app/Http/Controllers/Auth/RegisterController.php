<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Str;

use Illuminate\Support\Facades\Mail;
use App\Mail\verifyUserByEmail;
use Brick\Math\BigInteger;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required','string', 'min:6', 'max:255', 'unique:users',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            ],
            'full_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => [
                'required',
                'string',
                'min:8',
                'confirmed',
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            'password_confirmation' => [
                'same:password'
            ],
            'dob' => ['required', 'date', 'before:-18 years'],
            'ic_number_prefix' => [Rule::in(['00' , '01' ,'30' ,'31' ,'50' , '51'])],
            'ic_number_suffix' => ['digits:6'],
            'mobileno' => ['required', 'string'],
            'role' => ['required', 'string'],
            'picture' => ['required', 'image', 'mimes:jpeg,png,jpg', 'max:5000']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $ic_number_prefix = $data['ic_number_prefix'];
        $ic_number_suffix = $data['ic_number_suffix'];
        $ic_number = $ic_number_prefix . '-' . $ic_number_suffix;

        if(request()->has('picture')){
            $pictureuploaded = request()->file('picture');
            $picturename = time() . '.' . $pictureuploaded->getClientOriginalExtension();
            $picturepath = public_path('/images/');
            $pictureuploaded->move($picturepath,$picturename);
            return User::create([
                'name' => $data['name'],
                'full_name' => $data['full_name'],
                'email' => $data['email'],
                // 'role' => User::ROLE_CUSTOMER,
                'role' => $data['role'],
                'dob' => $data['dob'],
                'picture' => '/images/' . $picturename,
                'ic_number' => $ic_number,
                'mobileno' => '673' . $data['mobileno'],
                'password' => Hash::make($data['password']),
            ]);
        }
        $user = User::create([
            'name' => $data['name'],
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            // 'role' => User::ROLE_CUSTOMER,
            'role' => $data['role'],
            'dob' => $data['dob'],
            'ic_number' => $ic_number,
            'mobileno' => '673' . $data['mobileno'],
            'password' => Hash::make($data['password']),
        ]);
        Mail::to($user->email);
        return $user;
    }
}
