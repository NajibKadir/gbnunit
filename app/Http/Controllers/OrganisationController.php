<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrganisationController extends Controller
{
    public function dashboard () {
        return view ('/organisation_dashboard');
    }
}
