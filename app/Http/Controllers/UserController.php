<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
  
  public function dashboard() {
    $users = User::all();


    return view('dashboard.users', [
      'users' => $users,
      ]);
    
    }
    //dlt
    public function delete_index(){
      $users = DB::select('SELECT * FROM users');      
      return view('dashboard.users',['users'=>$users]);
    }

    public function destroy($id){
    $users = User::findOrFail($id);
    $users->delete();
    return redirect('users')->with('success', 'User deleted successfully');
}
    
    public function search(Request $request){
    $search = $request->get('search');
    $users = User::where('name','like'. '%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%')->orWhere('id','LIKE','%'.$search.'%')
    ->orWhere('role','LIKE','%'.$search.'%')->get();
    if(count($users)>0){
      return view('dashboard/users',['users' => $users])->withQuery('$search');
    }else {
      return view('dashboard/users',['users' => $users])->withMessage('User not found.');
    }
    }

}
