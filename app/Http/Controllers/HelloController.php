<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function helloWorld()
    {
        return "Hello world";
    }

    public function ntah()
    {
        return "ntahs";
    }
}
