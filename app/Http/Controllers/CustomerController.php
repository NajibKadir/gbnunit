<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function dashboard() {
        // return "hello, i am customer controller";
        return view ('/customer_dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', [
            'user' => $user
        ]);
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // echo $id;
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    public function update($id){

        $user = User::find($id);

        $user->update($this->validateUser());

        return redirect('/user/show' . $user->id);

    }

    protected function validateUser(){

        return request()->validate([
            'username' => 'required',
            'email' => 'required',
            'ic number' => 'required',
            'phone no' => 'required',
            'date of birth' => 'required'
        ]);

    }

}
