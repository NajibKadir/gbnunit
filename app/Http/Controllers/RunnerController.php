<?php

namespace App\Http\Controllers;

use App\Order;

use Illuminate\Http\Request;

class RunnerController extends Controller
{
    public function dashboard () {
        $orders = Order::all();
        
        return view ('dashboard.runner' , [
            'orders' => $orders,
        ]);
    
    }
}
