<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('orders.index', [
            'orders' => $orders
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Order $order)
    {
        return view('orders.show', [
            'order' => $order
        ]);
    }

    public function create($id)
    {
        return view('orders.create', [
            'id' => $id
        ]);
    }


    public function store(Request $request){
        
        $order = new Order();
        $order->product_id = $request->product_id;
        $order->delivery_address = $request->delivery_address;
        $order->pickup_address = $request->pickup_address;
        $order->phone_no = $request->phone_no;
        $order->customer_id = auth()->user()->id;
        $order->type_of_payment = $request->type_of_payment;
        $order->status = 'pending';
        $order->save();
<<<<<<< Updated upstream
        return redirect('/orders');
=======
        return redirect('orders');


>>>>>>> Stashed changes

    }

    public function search(Request $request){
        $search = $request->get('search');
        $orders = Order::where('pickup_address','like', '%'.$search.'%')->orWhere('delivery_address','LIKE','%'.$search.'%')->orWhere('status','LIKE','%'.$search.'%')
        ->orWhere('phone_no','LIKE','%'.$search.'%')->get();
        if(count($orders)>0){
          return view('orders/index',['orders' => $orders])->withQuery('$search');
        }else {
          return view('orders/index',['orders' => $orders])->withMessage('No data found.');
        }
        }
}
