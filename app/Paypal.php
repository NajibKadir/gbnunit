<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    public $fillable = ['name','details','price'];
}
