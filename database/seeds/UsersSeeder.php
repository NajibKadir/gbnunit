<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();

        $user1->name = 'samuel';
        $user1->full_name = 'samuel';
        $user1->email = 'sam@gmail.com';
        $user1->role = User::ROLE_ADMIN;
        $user1->ic_number = '01-909090';
        $user1->mobileno = '7856756';
        $user1->dob = '1997-09-09';
        $user1->email_verified_at = '2020-09-14 03:11:06.000000';
        $user1->password = Hash::make('12345678');
        $user1->remember_token = null;
        $user1->save();

        $user2 = new User();

        $user2->name = 'aziz';
        $user2->full_name = 'aziz';
        $user2->email = 'aziz@gmail.com';
        $user2->role = User::ROLE_CUSTOMER;
        $user2->ic_number = '01-099090';
        $user2->mobileno = '7859988';
        $user2->dob = '1997-09-09';
        $user2->email_verified_at = '2020-09-14 03:13:06.000000';
        $user2->password = Hash::make('12345678');
        $user2->remember_token = null;
        $user2->save();

        $user3 = new User();

        $user3->name = 'najib';
        $user3->full_name = 'najib';
        $user3->email = 'najib@gmail.com';
        $user3->role = User::ROLE_ORGANISATION;
        $user3->ic_number = '01-121212';
        $user3->mobileno = '7854567';
        $user3->dob = '1997-09-09';
        $user3->email_verified_at = '2020-09-15 03:29:20.000000';
        $user3->password = Hash::make('12345678');
        $user3->remember_token = null;
        $user3->save();

        $user4 = new User();

        $user4->name = 'anis';
        $user4->full_name = 'anis';
        $user4->email = 'anis@gmail.com';
        $user4->role = User::ROLE_RUNNER;
        $user4->ic_number = '01-232323';
        $user4->mobileno = '7859955';
        $user4->dob = '1997-09-09';
        $user4->email_verified_at = '2020-09-15 13:11:06.000000';
        $user4->password = Hash::make('12345678');
        $user4->remember_token = null;
        $user4->save();
    }
}
