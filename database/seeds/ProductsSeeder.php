<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new Product();

        $product->name = 'Package A';
        $product->price = 10;
        $product->description = "This is product A";
        $product->save();

        $productB = new Product();

        $productB->name = 'Package B';
        $productB->price = 15;
        $productB->description = "This is product B";
        $productB->save();

        $productC = new Product();

        $productC->name = 'Package C';
        $productC->price = 20;
        $productC->description = "This is product C";
        $productC->save();
    }
}
