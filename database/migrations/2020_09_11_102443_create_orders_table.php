<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->biginteger('customer_id');
            $table->biginteger('product_id');
            $table->string('pickup_address');
            $table->string('delivery_address');
            $table->biginteger('phone_no');
            $table->biginteger('deliverer_id')->nullable();
            $table->date('deliver_on')->nullable();
            $table->date('deliver_at')->nullable();
            $table->string('package_type')->nullable();
            $table->biginteger('total_price')->nullable();
            $table->string('type_of_payment')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
