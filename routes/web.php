<?php

use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Events\SendLocation;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('welcome');});
Route::get('/about', function () {return view('about');});
Route::get('/users', function () {
   $user=DB::table('users')->get();
   return view('dashboard/Users', ['users'=>$user]);
});
Route::get('/termsandcondition', function () {return view('termsandcondition');});
Route::get('/geolocation', function () {return view('geolocation');});
Route::get('/howtoplay', function () {return view('howtoplay');});

//Login
Route::get('/login', function () {return view('login');});

// Register
Route::get('/register', function () {return view('register');});

Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('index')->middleware('auth');
// Route::get('verify/{email}/{token}', 'Auth\RegisterController@verifyemail')->name('verify');

Route::get('/customer_dashboard', 'CustomerController@dashboard')->middleware('auth');
Route::get('/customer_dashboard/{user}', 'CustomerController@show')->middleware('auth');
Route::get('/admin_dashboard', 'AdminController@dashboard')->middleware('auth');
Route::get('/runner_dashboard', 'RunnerController@dashboard')->middleware('auth');
Route::get('/organisation_dashboard', 'OrganisationController@dashboard')->middleware('auth');


Route::get('/delete-user' , 'UserController@delete_index');
Route::delete('/users/{id}' , 'UserController@destroy')->name('users.destroy');
Route::get('/users/{id}/edit', 'CustomerController@edit')->middleware('auth');
Route::get('/users/{id}/update', 'CustomerController@update')->middleware('auth');
Route::put('/users/{id}', 'CustomerController@update')->middleware('auth');
Route::any('/users/search','UserController@search');

Route::get('/home', 'HomeController@index')->name('index')->middleware('auth');

// products
Route::get('/select_product', 'CustomerProductController@index')->middleware('auth');
Route::get('/products', 'ProductController@index')->middleware('auth');
Route::get('/products/create', 'ProductController@create')->middleware('auth');
Route::get('/products/{product}', 'ProductController@show');
Route::post('/products', 'ProductController@store');
Route::get('/products/{id}/edit', 'ProductController@edit');
Route::post('/products/{id}/update', 'ProductController@update');
Route::delete('/products/{id}', 'ProductController@destroy');


Route::get('/orders/create/{id}', 'OrderController@create')->middleware('auth');
Route::get('/orders', 'OrderController@index')->middleware('auth');
Route::post('/orders/{id}/picked' , 'OrderDeliveryController@picked');
Route::post('/orders/{id}/delivered' , 'OrderDeliveryController@delivered');
// Route::get('/orders/{order}', 'OrderController@show');
Route::post('/orders' , 'OrderController@store');
Route::get('/orders/customerorder' ,  'OrderController@show');
Route::any('/search','OrderController@search');
Route::any('/admin_dashboard/search','AdminController@search');

<<<<<<< Updated upstream
Route::get('payment-status',array('as'=>'payment.status','uses'=>'PaymentController@paymentInfo'));
Route::get('payment/{id}',array('as'=>'payment','uses'=>'PaymentController@payment'));
Route::get('payment-cancel', function () {
   return 'Payment has been canceled';
});

Route::post('/map', function (Request $request) {
    $lat = $request->input('lat');
    $long = $request->input('long');
    $location = ["lat"=>$lat, "long"=>$long];
    event(new SendLocation($location));
    return response()->json(['status'=>'success', 'data'=>$location]);
});
=======

//payment form
Route::get('/paypal', 'PaymentController@index');

// route for processing payment
Route::post('paypal', 'PaymentController@payWithpaypal');

// route for check status of the payment
Route::get('status', 'PaymentController@getPaymentStatus');
>>>>>>> Stashed changes
