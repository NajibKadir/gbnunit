@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> {{ isset($url) ? ucwords($url) : ""}} {{ __('Register') }}</div>

                <div class="card-body">
                    @isset($url)
                    <form method="POST" action='{{ url("register/$url") }}' aria-label="{{ __('Register') }}">
                    @else
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
                    @endisset
                        @csrf
                        {{-- username  --}}
                        <label style="color: red">* Required Field</label>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username *') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="example" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- email  --}}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@mail.com" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- ic number  --}}
                        <div class="form-group row">
                            <label for="icnumber" class="col-md-4 col-form-label text-md-right">{{ __('Identity Card No. *') }}</label>

                            <div class="col-md-6">
                                <input id="icnumber" type="text" class="form-control @error('icnumber') is-invalid @enderror" name="icnumber"
                                value="{{ old('icnumber') }}" required autocomplete="icnumber" placeholder="01-234567" pattern="[0-9]{2}-[0-9]{6}">

                                @error('icnumber')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- phone no  --}}
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone No. *') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone"
                                value="{{ old('phone') }}" required autocomplete="phone" placeholder="123 4567" pattern="[0-9]{3} [0-9]{4}">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- date of birth  --}}
                        {{-- <script type="text/javascript">
                        $('.birthdate-click').click(function(e) {
                        e.preventDefault();
                        $month = $('.month').val();
                        $day = $('.day').val();
                        $year = $('.year').val();
                        if (ageValidation(18, $month, $day, $year)) {
                            alert('sorry not old enough');
                        } else alert('old enough');

                        });

                        function ageValidation(age, birthMonth, birthDate, birthYear) {
                        var a = age;
                        var todaysDate = new Date();
                        todaysDate.setFullYear(todaysDate.getFullYear() - a);
                        var usersBirthDay = new Date();
                        var bm = birthMonth;
                        var bd = birthDate;
                        var by = birthYear;
                        usersBirthDay.setFullYear(by, bm - 1, bd);
                        if ((todaysDate - usersBirthDay) < 0) {
                            return true;
                        } else return false
                        }
                        </script>
                        <div class="form-group row">
                            <label for="birth-date"class="col-md-4 col-form-label text-md-right">Birth Date (mm/dd/yyyy)</label>
                            <div class="col-md-6">
                            <input class="month form-control" type="text" maxlength="2" name="birth_month" placeholder="Month" required>
                            <input class="day form-control" type="text" maxlength="2" name="birth_day" placeholder="Day" required>
                            <input class="year form-control" maxlength="4" type="text" name="birth_year" placeholder="Year" required>
                            <button class="birthdate-click">Check age</a>
                        </div>
                    </div> --}}

                    <div class="form-group row">
                        <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth. *') }}</label>

                        <div class="col-md-6">
                            <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob"
                            value="{{ old('dob') }}" required autocomplete="dob" placeholder="dd/mm/yy">

                            @error('dob')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                        {{-- password  --}}
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password *') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="your password" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{-- confirm password  --}}
                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password *') }}</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" placeholder="confirm your password" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        {{-- profile picture  --}}
                        <script type='text/javascript'>
                            function preview_image(event)
                            {
                             var reader = new FileReader();
                             reader.onload = function()
                             {
                              var output = document.getElementById('output_image');
                              output.src = reader.result;
                             }
                             reader.readAsDataURL(event.target.files[0]);
                            }
                        </script>
                        <div class="form-group row">
                            <label for="image" class="col-md-4 col-form-label text-md-right">Profile Image *</label>
                            <div class="col-md-6">
                                {{-- preview image  --}}
                            <div id="wrapper">
                                <input name="image" class="form-control" type="file" accept="image/*" onchange="preview_image(event)" required>
                                <img id="output_image" class="img-responsive center-block d-block mx-auto" margin="200" width="200" />
                            </div>
                            </div>
                        </div>

                        {{-- terms and condition  --}}
                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="tnc" id="tnc" {{ old('tnc') ? 'checked' : '' }} required>
                                    <label class="form-check-label" for="tnc">
                                        I agree to the <a href="/termsncondition">Terms of Service</a> and <a href="/privacypolicy">Privacy Policy. *</a>
                                    </label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                    <button type="submit" id="submit" class="btn btn-primary btn-block">
                                        {{ __('Register') }}
                                    </button>
                                    {{-- <input type="submit" id="submit" value="submit" disabled="disabled" /> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <script type='text/javascript'>
    (function() {
    $('form > input').keyup(function() {

        var empty = false;
        $('form > input').each(function() {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#submit').attr('disabled', 'disabled');
        } else {
            $('#submit').removeAttr('disabled');
        }
    });
})()

/* instead of form we can use the IDs of certain fields only. In case not all fields are mandatory */
</script> --}}
@endsection


