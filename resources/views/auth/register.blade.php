@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="card">
                <div class="card-header"> {{ isset($url) ? ucwords($url) : ""}} {{ __('Register') }}</div>

                <div class="card-body">
                    @isset($url)
                    <form method="POST" action='{{ url("register/$url") }}' aria-label="{{ __('Register') }}">
                        @else
                        <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
                            @endisset
                            @csrf
                            {{-- Full name please  --}}
                            <label style="color: red">* Required Field</label>
                            <div class="form-group row">
                                <label for="full_name" class="col-md-4 col-form-label text-md-right">{{ __('Full Name *') }}</label>

                                <div class="col-md-6">
                                    <small class="form-text text-muted">Fullname as appears on Smart Card Identity.</small>
                                    <input id="full_names" type="text" class="form-control  @error('full_name') is-invalid @enderror" placeholder="Please Enter Your Name" name="full_name" value="{{ old('full_name') }}" required autocomplete="full_name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            {{-- username  --}}

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username *') }}</label>

                                <div class="col-md-6">
                                    <small class="form-text text-muted">Username must include at least: 8 or more character, one lowercase letter, one uppercase letter, one digit. This has to be unique for all of our customer.</small>
                                    <input id="name" type="text" class="form-control  @error('name') is-invalid @enderror" placeholder="Please Enter Your Username" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- email  --}}
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>

                                <div class="col-md-6">
                                    <small class="form-text text-muted">Enter your valid email address. Note: once you click submit button there is no turning back to enter another email address.</small>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="example@email.com" name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- role  --}}
                            <div class="form-group row">
                                <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role *') }}</label>

                                <div class="col-md-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="role" id="role1" value="customer">
                                        <label class="form-check-label" for="role1">Individual</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="role" id="role2" value="organisation">
                                        <label class="form-check-label" for="role2">Organisation</label>
                                      </div>
                                      <br>
                                      <small id="demo" role="alert" style="color: red;"></small>
                                      @error('role')
                                      <span class="invalid-feedback" role="alert">
                                        <strong>
                                            <script>
                                                document.getElementById("demo").innerHTML = "Please choose your role to register as.";
                                            </script>
                                        </strong>
                                    </span>
                                      @enderror
                                </div>
                            </div>



                            {{-- ic number  --}}
                            <div class="form-group row">
                                <label for="ic_number" class="col-md-4 col-form-label text-md-right">{{ __('Identity Card No. *') }}</label>

                                <div class="col-md-6">
                                    <small class="form-text text-muted">Enter your Smart Card Identity number.</small>
                                    <select name="ic_number_prefix">
                                        <option value="00">00</option>
                                        <option value="01">01</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                        <option value="50">50</option>
                                        <option value="51">51</option>
                                    </select>
                                    @error('ic_number_prefix')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    -
                                    <input type="text" name="ic_number_suffix" maxlength="6">
                                </div>
                            </div>

                            {{-- mobile no  --}}
                            <div class="form-group row">
                                <label for="mobileno" class="col-md-4 col-form-label text-md-right">{{ __('Mobile No. *') }}</label>

                                <div class="col-md-6">
                                    <small class="form-text text-muted">Enter your valid mobile number.</small>
                                    <input id="mobileno" type="text" class="form-control  @error('mobileno') is-invalid @enderror" placeholder="Please Enter Your mobile no" name="mobileno" maxlength="7" required autocomplete="mobileno">
                                    @error('mobileno')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            {{-- date of birth  --}}
                                <div class="form-group row">
                                    <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth. *') }}</label>

                                    <div class="col-md-6">
                                        <small class="form-text text-muted">Note: Below age 18 years old must have parent's consent to register.</small>
                                        <input id="dob" type="date" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" required autocomplete="dob" placeholder="dd/mm/yy">
                                        @error('dob')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- password  --}}
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password *') }}</label>

                                    <div class="col-md-6">
                                        <small class="form-text text-muted">Enter your password with at least: 8 or more character, one lowercase letter, one uppercase letter, one digit and one special character.</small>
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="your password" name="password" required autocomplete="new-password">
                                        <div class="col-right" style="margin-top: 5px;">
                                            <input id="show" type="checkbox" onclick="myFunction()"> <label for="show">Show Password</label>
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message = "Your password must include at least: 8 or more character, one lowercase letter, one uppercase letter, one digit and one special character" }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- confirm password  --}}
                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password *') }}</label>
                                    <div class="col-md-6">
                                        <input id="password-confirm" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="confirm your password" name="password_confirmation" required autocomplete="new-password">
                                        @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message = "Your password does not match." }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                {{-- profile picture  --}}
                                <div class="form-group row">
                                    <label for="picture" class="col-md-4 col-form-label text-md-right">Profile Image *</label>
                                    <div class="col-md-6">
                                        {{-- preview image  --}}
                                        <div id="wrapper">
                                            <small class="form-text text-muted">Image format: jpg, jpeg, png only.</small>
                                            <small class="form-text text-muted">Max Uploaded Size: 5 MB.</small>
                                            <input name="picture" class="form-control" type="file" accept="image/*" onchange="preview_image(event)" required>
                                            <img id="output_image" class="img-responsive center-block d-block mx-auto" style="width: 151px; height:196px;"/>
                                        </div>
                                        <small id="demo" role="alert" style="color: red;"></small>
                                    </div>
                                    @error('picture')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>
                                                <script>
                                                    document.getElementById("demo").innerHTML = "Your picture must be an image with format: jpg, jpeg, png only";
                                                </script>
                                            </strong>
                                        </span>
                                    @enderror
                                </div>

                                {{-- terms and condition  --}}
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="tnc" id="tnc" {{ old('tnc') ? 'checked' : '' }} required>
                                            <label class="form-check-label" for="tnc">
                                                I agree to the <a href="/termsandcondition">Terms and Condition. *</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" id="submit" class="btn btn-primary btn-block">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
