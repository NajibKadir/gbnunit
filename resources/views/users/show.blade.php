@extends('layouts.customerapp')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Profile</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <div class="col">
                                <center>
                                    <img src="" alt="" width="250" height="250">
                                </center>
                            </div>
                        </div>
                        {{-- username  --}}
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Username:') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" placeholder="example" name="name" value="{{ $user->name }}" readonly>
                            </div>
                        </div>

                        {{-- email  --}}
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address:') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control"  name="email" value="{{ $user->email }}" readonly>

                            </div>
                        </div>

                        {{-- ic number  --}}
                        <div class="form-group row">
                            <label for="icnumber" class="col-md-4 col-form-label text-md-right">{{ __('Identity Card No. *') }}</label>

                            <div class="col-md-6">
                                <input id="icnumber" type="text" class="form-control" name="icnumber"
                                value="{{ old('icnumber') }}" required autocomplete="icnumber" placeholder="01-234567" readonly>
                            </div>
                        </div>

                        {{-- phone no  --}}
                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone No. *') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="tel" class="form-control" name="phone"
                                value="{{ $user->phone }}" placeholder="123 4567" readonly>
                            </div>
                        </div>

                        {{-- date of birth  --}}
                        {{-- <script type="text/javascript">
                        $('.birthdate-click').click(function(e) {
                        e.preventDefault();
                        $month = $('.month').val();
                        $day = $('.day').val();
                        $year = $('.year').val();
                        if (ageValidation(18, $month, $day, $year)) {
                            alert('sorry not old enough');
                        } else alert('old enough');

                        });

                        function ageValidation(age, birthMonth, birthDate, birthYear) {
                        var a = age;
                        var todaysDate = new Date();
                        todaysDate.setFullYear(todaysDate.getFullYear() - a);
                        var usersBirthDay = new Date();
                        var bm = birthMonth;
                        var bd = birthDate;
                        var by = birthYear;
                        usersBirthDay.setFullYear(by, bm - 1, bd);
                        if ((todaysDate - usersBirthDay) < 0) {
                            return true;
                        } else return false
                        }
                        </script>
                        <div class="form-group row">
                            <label for="birth-date"class="col-md-4 col-form-label text-md-right">Birth Date (mm/dd/yyyy)</label>
                            <div class="col-md-6">
                            <input class="month form-control" type="text" maxlength="2" name="birth_month" placeholder="Month" required>
                            <input class="day form-control" type="text" maxlength="2" name="birth_day" placeholder="Day" required>
                            <input class="year form-control" maxlength="4" type="text" name="birth_year" placeholder="Year" required>
                            <button class="birthdate-click">Check age</a>
                        </div>
                    </div> --}}

                    <div class="form-group row">
                        <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth. *') }}</label>

                        <div class="col-md-6">
                            <input id="dob" type="date" class="form-control" name="dob"
                            value="{{ $user->dateofbirth }}" placeholder="dd/mm/yy" readonly>
                        </div>
                    </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="/users/{{$user->id}}/edit" class="btn btn-primary btn-lg btn-block">Edit</a>
                                    {{-- <input type="submit" id="submit" value="submit" disabled="disabled" /> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
