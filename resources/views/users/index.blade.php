@extends('layouts.newadmin')

@section('content')
<div class="container">
    @auth
        <a class="btn btn-primary" href="{{ url('products/create') }}">Create New Product</a>
    @endauth
    <ul class="list-group">
        @foreach($users as $user)
            <li class="list-group-item"><a href="{{ url('products/' . $product->id) }}">{{ $product->name }}</a></li>
            
        @endforeach
    </ul>
</div>

@endsection
