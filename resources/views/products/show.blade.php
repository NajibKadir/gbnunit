@extends('layouts.app')

@section('content')
<div class="container">
    <strong><label for="">Name:</label></strong>
    <span>{{ $product->name }}</span><br>
    <strong><label for="">Price:</label></strong>
    <span>{{ $product->price }}</span><br>
    <strong><label for="">Description</label></strong>
    <span>{{ $product->description }}</span><br>
</div>
<a href="/products/{{$product->id}}/edit" class="btn btn-default">Edit</a>
<form action="/products/{{$product->id}}" method="post">
    @csrf
    @method('DELETE')
    <button class="btn btn-sm btn-danger">Delete</button>
</form>
@endsection
