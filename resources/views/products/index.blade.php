@extends('layouts.newadmin')

@section('content')
<div class="container">
    {{-- @auth
        <a class="btn btn-primary" href="{{ url('orders/new') }}">Create New Order</a><br>
    @endauth --}}
    <ul class="list-group">
        @foreach($products as $product)
            {{-- <li class="list-group-item"><a href="{{ url('products/' . $product->id) }}">{{ $product->name }}</a></li> --}}
            <div class="card">
                <h5 class="card-header">{{ $product->name }}</h5>
                <div class="card-body">
                <h5 class="card-title">Price: {{ $product->price }}</h5>
                <h5 class="card-title"> {{ $product->description }}</h5>
                  <a class="btn btn-primary" href="{{ url('products/' . $product->id . '/edit') }}">Edit</a>

                 
                </div>
              </div>
              <p></p>
        @endforeach
    </ul>
</div>

@endsection
