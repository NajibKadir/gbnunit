@extends('layouts.newadmin')

@section('content')
<div class="container">

    <form action="{{ url('products', $product->id) }}/update" method="POST">
        @csrf
      
        <strong><label for="">Name:</label></strong>
    <input type="text" name="name" value="{{ old('name') ?? $product->name }}" class="form-control">
        <strong><label for="">Price:</label></strong>
        <input type="number" name="price" value="{{ old('price') ?? $product->price }}" class="form-control">
        <strong><label for="">Description:</label></strong>
        <textarea name="description" class="form-control">{{ old('description') ?? $product->description }}</textarea>
        <button type="submit" class="btn btn-primary mt-2">Update</button>
    </form>
</div>

@endsection
