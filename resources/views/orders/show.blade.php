@extends('layouts.app')

@section('content')
<div class="container">
    <strong><label for="">Name:</label></strong>
    <span>{{ $order->name }}</span><br>
    <strong><label for="">Deliverer:</label></strong>
    <span>{{ $order->deliverer }}</span><br>
</div>
{{-- <a href="/products/{{$product->id}}/edit" class="btn btn-default">Edit</a>
<form action="/products/{{$product->id}}" method="post">
    @csrf
    @method('DELETE')
    <button class="btn btn-sm btn-danger">Delete</button>
</form> --}}
<table class="table">

    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Customer</th>
        <th scope="col">Address</th>
        <th scope="col">Deliverer</th>
      </tr>
    </thead>
    @foreach($orders as $order)
    <tbody>
      <tr>
      <th scope="row">{{$order->id}}</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
      </tr>
    </tbody>
    @endforeach
  </table>


@endsection
