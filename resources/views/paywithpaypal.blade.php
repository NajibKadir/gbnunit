<html>
<head>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>
<body>
    <div class="w3-container">
        @if ($message = Session::get('success'))
        <div class="w3-panel w3-green w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
    				class="w3-button w3-green w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('success');?>
        @endif

        @if ($message = Session::get('error'))
        <div class="w3-panel w3-red w3-display-container">
            <span onclick="this.parentElement.style.display='none'"
    				class="w3-button w3-red w3-large w3-display-topright">&times;</span>
            <p>{!! $message !!}</p>
        </div>
        <?php Session::forget('error');?>
        @endif

    	<form class="w3-container w3-display-middle w3-card-4 w3-padding-16" method="POST" id="payment-form"
          action="{!! URL::to('paypal') !!}">
    	  <div class="w3-container w3-blue w3-padding-16">Pay with Paypal</div>
    	  {{ csrf_field() }}
    	  <h2 class="w3-text-blue">Payment Details</h2>


          <div class="invoice-box">
        <table cellpadding="0" cellspacing="-2">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            
                            <td>
                                Invoice #: 123<br>
                                Created: January 1, 2015<br>
                                Due: February 1, 2015
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                GBN-Unit, Inc.<br>
                                Block 2E Ong Sum Ping Condominium,<br>
                                Bandar Seri Begawan BA1311
                            </td>
                            
                            <td>
                                Organization.inc<br>
                                test<br>
                                test@example.com
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Package 1 (Delivery services)
                </td>
                
                <td>
                    $5
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    Extra Quantity ($1 per item)
                </td>
                
                <td>
                    $2
                </td>
            </tr>
            
            <tr class="item last">
                <td>
                    Extra Weight ($1/100g)
                </td>
                
                <td>
                    $1
                </td>
            </tr>
            
            <tr class="total">
                <td></td>
                
                <td>
                   Total: $8
                </td>
            </tr>
        </table>
    </div>

<br>

<div id='gcw_mainF7D4FShRM' class='gcw_mainF7D4FShRM'></div>
<a id='gcw_siteF7D4FShRM' href='https://freecurrencyrates.com/en/'></a>
<script>function reloadF7D4FShRM(){ var sc = document.getElementById('scF7D4FShRM');if (sc) sc.parentNode.removeChild(sc);sc = document.createElement('script');sc.type = 'text/javascript';sc.charset = 'UTF-8';sc.async = true;sc.id='scF7D4FShRM';sc.src = 'https://freecurrencyrates.com/en/widget-horizontal?iso=BND-USD&df=1&p=F7D4FShRM&v=fits&source=fcr&width=500&width_title=225&firstrowvalue=1&thm=A6C9E2,FCFDFD,4297D7,5C9CCC,FFFFFF,C5DBEC,FCFDFD,2E6E9E,000000&title=Currency%20Converter&tzo=-480';var div = document.getElementById('gcw_mainF7D4FShRM');div.parentNode.insertBefore(sc, div);} reloadF7D4FShRM(); </script>
<!-- put custom styles here: .gcw_mainF7D4FShRM{}, .gcw_headerF7D4FShRM{}, .gcw_ratesF7D4FShRM{}, .gcw_sourceF7D4FShRM{} -->
    	 <br> <label class="w3-text-blue"><b>Enter Amount:</b></label>
    	  <input class="w3-input w3-border" id="amount" type="text" name="amount"></p>
    	  <button class="w3-btn w3-blue">Pay with PayPal</button>
        <a class="w3-btn w3-red" href="/orders">Cancel</a>
    	</form>
    </div>
</body>
</html>