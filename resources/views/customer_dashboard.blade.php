@extends('layouts.customerapp')

@section('content')
<div class="container">
    <div class="row justify-content-center">
     
            <div class="card">
                <div class="card-header">Customer Dashboard</div>

                <div class="card-body">
                     Hi there, {{auth()->user()->name}}.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
