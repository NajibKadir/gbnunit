@extends('layouts.runnerapp')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Runner Dashboard</div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <th scope="col">Name Of Customer</th>
                            <th scope="col">Address Of Pickup</th>
                            <th scope="col">Delivered Address</th>
                            <th scope="col">Customer Phone NO</th>
                            <th scope="col">Total Price</th>
                            <th scope="col">Package Type</th>
                            @if(Auth::user()->role == 'runner')
                            <th scope="col">Please Take This Pending Order</th>
                            @endif 
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>   {{$order->customer->name}} </td>
                                <td>   {{$order->pickup_address}}  </td>
                                <td>   {{$order->delivery_address}} </td>
                                <td>   {{$order->phone_no}} </td>
                                
                              
                                @if(Auth::user()->role == 'runner')
                                <td>
                                    @if($order->status == 'pending')
                                    <form action="{{url('orders/' .$order->id. '/picked')}}" method="POST">
                                        @csrf
                                        <button>Take Order</button> 
                                    </form>
                                    @elseif($order->status == 'picked')
                                    <form action="{{url('orders/' .$order->id. '/delivered')}}" method="POST">
                                        @csrf
                                        <button>Delivered</button> 
                                    </form>
                                    @elseif($order->status == 'delivered')
                                    
                                    Delivered at {{$order->deliver_at->format('d/M/Y')}}

                                    @endif

                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection