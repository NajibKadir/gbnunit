@extends('layouts.newadmin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card-header" align="center">Customer Order Status</div>
        <div class="col-md-14">
                    <form action="/admin_dashboard/search" method="get" role="search">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search Address, Phone Number or Order Status">
                    <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary">Search
                        <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
            </div>
            <table class="table table-bordered">
            <thead>
            <th scope="col">Customer ID</th>
            <th scope="col">Address Of Pickup</th>
            <th scope="col">Delivered Address</th>
            <th scope="col">Customer Phone NO</th>
            <th scope="col">Total Price</th>
            <th scope="col">Package Type</th>
            <th scope="col">Status</th>
            <th scope="col">Driver who took this order</th>
             <tbody>
                    @foreach($orders as $order)
                              <tr>
                                  <th>{{$order->customer_id}}</th>
                                  <th>{{$order->pickup_address}}</th>
                                  <th>{{$order->delivery_address}}</th>
                                  <th>{{$order->phone_no}}</th>
                                  <!-- not yet done -->
                                  <th>{{$order->price}}</th>
                                  <th>{{$order->product_id}}</th>
                                  <th>{{$order->status}}</th>
                                  <th>{{$order->runner}}</th>
                                  <!-- not yet done -->
                                </tr>
                    @endforeach
                    </tbody>
                </thead>      
                
         
      
      
      
        </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
