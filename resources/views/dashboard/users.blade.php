@extends('layouts.newadmin')
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit or Delete Users</div>
                <div class="col-md-6">
                    <form action="/users/search" method="get" role="search">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search ID, Names or Role">
                    <span class="input-group-prepend">
                        <button type="submit" class="btn btn-primary">Search
                        <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </form>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">Action</th>
                          <tbody>
                              @foreach($users as $user => $data)
                              <tr>
                                  <th>{{$data->id}}</th>
                                  <th>{{$data->name}}</th>
                                  <th>{{$data->email}}</th>
                                  <th>{{$data->role}}</th>
                                  <th><form method="POST" action="{{ route('users.destroy', [$data->id]) }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger" onclick="return confirm('Delete this user?')">Delete</button>
                                        </form></th>
                              </tr>
                              @endforeach
                          </tbody>
                           
                            
                            
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection