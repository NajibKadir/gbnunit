<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>GBN-Unit</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                    <a href="{{ url('/about') }}">About</a>
                        <a href="/{{ Auth::user()->role }}_dashboard">Dashboard</a>
                        <a class="nav-link" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a href="{{ url('login') }}">Login</a>


                            <a href="{{ url('register') }}">Register</a>

                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                <br>
                About Us.
                </div>
                <h3>The Central</h3>
                <h3>Affordable . Safe . Secure</h3>
                <div class="text-center">
                    <img src="https://i0.wp.com/theicon.ist/wp-content/uploads/2018/09/LGX-Team-2.jpg?resize=1280%2C640&ssl=1" width="400" height="200" class="rounded" alt="...">
                    <p>We are from a company called “GBN-Unit “or" Goverment Brunei Unit</p>
                    <p>We are a company that established in 2020 . </p>
                    <p>Our Price are revised and we making it affordable for all people</p>
                    <p>We also one of the first in which making the runner centralize</p>
                    
                        
                </div>

            </div>

        </div>
    </body>
</html>
